NAME 	 = boda
VERSION  = $(shell git describe --tags --always --dirty)
BRANCH   = $(shell git rev-parse --abbrev-ref HEAD)

build: options
	docker build -t $(NAME) . \
		--build-arg VERSION=$(VERSION) \
		--build-arg BRANCH=$(BRANCH)

run:
	docker run --rm -p 8080:8080 $(NAME)

options:
	@echo $(NAME) build options:
	@echo "VERSION   = ${VERSION}"
	@echo "BRANCH    = ${BRANCH}"