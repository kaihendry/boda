package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"
)

var (
	Version   string
	Branch    string
	GoVersion = runtime.Version()
)

func main() {
	log.Println("Version:", Version, "Branch:", Branch, "GoVersion:", GoVersion)
	// Allow PORT to be overridden by environment variable
	PORT := 8080
	if port, ok := os.LookupEnv("PORT"); ok {
		PORT, _ = strconv.Atoi(port)
	}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Version", fmt.Sprintf("Version: %s Branch: %s GoVersion: %s", Version, Branch, GoVersion))
		fmt.Fprintf(w, "Hello %s\n", time.Now())
	})
	log.Println("Listening on port", PORT)
	http.ListenAndServe(fmt.Sprintf(":%d", PORT), nil)
}
